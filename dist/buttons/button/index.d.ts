import { h, JSX } from 'preact';
export interface ButtonProps {
    children: string | JSX.Element;
    href?: string;
    onClick?: (event: MouseEvent) => void;
}
export declare const Button: ({ children, href, onClick }: ButtonProps) => h.JSX.Element;
//# sourceMappingURL=index.d.ts.map