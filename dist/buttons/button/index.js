"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Button = void 0;
const preact_1 = require("preact");
const preact_2 = require("jsxstyle/preact");
const Button = ({ children, href, onClick }) => {
    return ((0, preact_1.h)(preact_2.Block, { component: "a", props: {
            href,
            onClick,
        }, padding: "12px", borderRadius: "6px", background: "#ccc" }, children));
};
exports.Button = Button;
//# sourceMappingURL=index.js.map