"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StyledButton = void 0;
const stylerun_1 = require("stylerun");
exports.StyledButton = (0, stylerun_1.styled)('a', ({ className }) => `
  .${className} {
    padding: 12px;
  }
  .${className}::before {
    content: '""';
    padding: 10px;
    background: red;
  }
`);
//# sourceMappingURL=style.js.map