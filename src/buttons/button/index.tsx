import { h, JSX } from 'preact';
import { Block } from 'jsxstyle/preact';

export interface ButtonProps {
  children: string | JSX.Element;
  href?: string;
  onClick?: (event: MouseEvent) => void;
}

export const Button = ({ children, href, onClick }: ButtonProps) => {
  return (
    <Block
      component="a"
      props={{
        href,
        onClick,
      }}
      padding="12px"
      borderRadius="6px"
      background="#ccc"
    >
      {children}
    </Block>
  );
};
